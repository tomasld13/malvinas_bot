##Configuración

1. Clonar el repositorio.
2. Instalar las dependencias usando 'pip install -r requirements.txt'
3. Probar usando telegram desktop ya que no funciona en web.

##Funcionalidades:

-/start Menu principal desplegable de opciones.
-/help Menu de ayuda, lista con comandos disponibles.
-/otro Si el bot no tiene configurado ningun comando para el mensaje ingresado se lo avisa al usuario y le hace saber del comando /help.