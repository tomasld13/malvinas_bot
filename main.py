import telebot
from telebot import types

#Token conexion con bot
TOKEN = "7179899284:AAGXdcmZv0tM9J29qRBqT7gDpZo_onOXo8Q"
bot = telebot.TeleBot(TOKEN)

#Comandos del BOT

#help
@bot.message_handler(commands=["help"])
def send_help(message):
    bot.reply_to(message, "Necesitas ayuda, dejame contarte todos los comandos que tengo para ayudarte:\n" + 
                 "/start - Despliega el menu principal")

#No se encuentra el comando
@bot.message_handler(func=lambda m: True)
def echo_all(message):
    bot.reply_to(message, "Lo siento, no he podido entender ese comando. En caso de que necesites ayuda puedes escribirme /help para que pueda ayudarte.")

#start
@bot.message_handler(commands=["start"])
def send_options(message):
    markup = types.InlineKeyboardMarkup(row_width=5)
    #Creacion de botones
    buttons = []
    #Primer parametro es el texto que aparecera en el btn, el segundo es el call.data
    buttons.append(types.InlineKeyboardButton("Municipio", callback_data="municipio"))
    buttons.append(types.InlineKeyboardButton("Hospital", callback_data="hospital"))
    buttons.append(types.InlineKeyboardButton("Zoonosis", callback_data="zoonosis"))
    buttons.append(types.InlineKeyboardButton("Empleos", callback_data="empleos"))
    buttons.append(types.InlineKeyboardButton("Tramites", callback_data="tramites"))

    #Agrego Buttons
    for button in buttons:
        markup.add(button)

    #Mensaje start
    bot.send_message(message.chat.id, "¿Con que puedo ayudarte?", reply_markup=markup)

#Recibe respuesta de los botones de /start
@bot.callback_query_handler(func=lambda call:True)
def callback_query(call):
    if(call.data == "municipio"):
        opciones_municipio(call)
    else:#Falta crear las demas opciones
        opciones_municipio(call)

def opciones_municipio(call):
    markup = types.InlineKeyboardMarkup(row_width=3)
    #Creacion de botones /Municipio ejemplo
    buttons = []
    buttons.append(types.InlineKeyboardButton("Horarios", callback_data="horarios"))
    buttons.append(types.InlineKeyboardButton("Direccion", callback_data="direccion"))
    buttons.append(types.InlineKeyboardButton("Medios de contacto", callback_data="contactos"))

    #Agrego Buttons
    for button in buttons:
        markup.add(button)

    #Mensaje Municipio
    bot.send_message(call.message.chat.id, "¿Que información del municipio estas buscando?", reply_markup=markup)

if __name__ == "__main__":
    bot.polling(none_stop=True)